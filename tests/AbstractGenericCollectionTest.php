<?php

namespace Fortress\TypeCollection\Tests;

use Fortress\TypeCollection\Tests\Resource\IntegerGenericCollection;
use PHPUnit\Framework\TestCase;

class AbstractGenericCollectionTest extends TestCase
{
    public function testCollectionCreationAcceptsTypeTrue()
    {
        $sut = new IntegerGenericCollection([1, 2]);

        $this->assertCount(2, $sut);
        $this->assertEquals([1, 2], $sut->all());
    }

    public function testCollectionCreationWithInvalidType()
    {
        $this->expectException(\UnexpectedValueException::class);
        $this->expectExceptionMessage(sprintf("Invalid value passed to %s", IntegerGenericCollection::class));

        new IntegerGenericCollection([1, 'This is a string!']);
    }

    public function testPushWithTypeTrue()
    {
        $sut = new IntegerGenericCollection;
        $sut->push(1);

        $this->assertCount(1, $sut);
        $this->assertEquals([1], $sut->all());
    }

    public function testPushWithInvalidType()
    {
        $this->expectException(\UnexpectedValueException::class);
        $this->expectExceptionMessage(sprintf("Invalid value passed to %s", IntegerGenericCollection::class));

        $sut = new IntegerGenericCollection;
        $sut->push('This is a string!');
    }

    public function testPutWithTypeTrue()
    {
        $sut = new IntegerGenericCollection;
        $sut->put('key', 1);

        $this->assertCount(1, $sut);
        $this->assertEquals(['key' => 1], $sut->all());
        $this->assertEquals(1, $sut->get('key'));
    }

    public function testPutWithInvalidType()
    {
        $this->expectException(\UnexpectedValueException::class);
        $this->expectExceptionMessage(sprintf("Invalid value passed to %s", IntegerGenericCollection::class));

        $sut = new IntegerGenericCollection;
        $sut->put('key', 'This is a string!');
    }

    public function testAddWithTypeTrue()
    {
        $sut = new IntegerGenericCollection;
        $sut->add(1);

        $this->assertCount(1, $sut);
        $this->assertEquals(1, $sut->first());
    }

    public function testAddWithInvalidTrue()
    {
        $this->expectException(\UnexpectedValueException::class);
        $this->expectExceptionMessage(sprintf("Invalid value passed to %s", IntegerGenericCollection::class));
        
        $sut = new IntegerGenericCollection;
        $sut->add('test');
    }
}
