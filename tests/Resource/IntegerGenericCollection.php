<?php

namespace Fortress\TypeCollection\Tests\Resource;

use Fortress\TypeCollection\AbstractGenericCollection;

class IntegerGenericCollection extends AbstractGenericCollection
{
    protected function willAcceptType($value): bool
    {
        return is_integer($value);
    }
}
