<?php

namespace Fortress\TypeCollection;

use Illuminate\Support\Collection;
use UnexpectedValueException;

/**
 * Class AbstractGenericCollection
 * @package Brash\TypeCollection
 */
abstract class AbstractGenericCollection extends Collection
{
    /**
     * AbstractGenericCollection constructor.
     *
     * @param array $items
     */
    public function __construct($items = [])
    {
        $items = $this->getArrayableItems($items);

        $this->validateValues($items);

        parent::__construct($items);
    }

    /**
     * @param array $items
     */
    private function validateValues(array $items)
    {
        foreach ($items as $item) {
            $this->validateValue($item);
        }
    }

    /**
     * @param $value
     */
    private function validateValue($value)
    {
        if (!$this->willAcceptType($value)) {
            throw new UnexpectedValueException(sprintf(
                "Invalid value passed to %s",
                get_class($this)
            ));
        }
    }

    /**
     * @param mixed $values [optional]
     *
     * @return $this
     */
    final public function push(...$values)
    {
        $this->validateValues($values);

        foreach ($values as $value) {
            $this->items[] = $value;
        }

        return $this;
    }

    /**
     * @param mixed $key
     * @param mixed $value
     *
     * @return $this
     */
    final public function put($key, $value)
    {
        $this->validateValue($value);

        return parent::put($key, $value);
    }

    /**
     * @param mixed $item
     * @return $this
     */
    final public function add($item)
    {
        $this->validateValue($item);

        return parent::add($item);
    }

    /**
     * @param $value
     *
     * @return bool
     */
    abstract protected function willAcceptType($value): bool;
}
